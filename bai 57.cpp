// sap xep mang hai chieu tang dan hoac giam dan
#include <stdio.h>
int a[100][100];
int m, n, k;

void nhapMaTran(int x[100][100], int &m, int &n){
	do{
		printf("Nhap vao m va n: ");
		scanf("%d%d", &m, &n);
	}while(m<=0 || n<=0);
	
	for(int i=0; i<m; i++){
		for(int j=0; j<n; j++){
			printf("x[%d][%d]=", i, j);
			scanf("%d", &x[i][j]);
		}
	}
}

void xuatMaTran(int x[100][100], int m, int n){
	printf("\nMang: \n");
	for(int i=0; i<m; i++){
		for(int j=0; j<n; j++){
			printf("%d\t", a[i][j]);
		}
		printf("\n");
	}
}
void mangTangDan(int x[100][100], int m, int n){
	int k =m*n;
	for(int i=0; i<k-1; i++){     //duyet het tat cac ca phan tu trong mang 1 chieu
		for(int j=i+1; j<k; j++){
			if(x[i/n][i%n] > x[j/n][j%n]){ //vi tri ch�ng ta dang dung o dau
			int temp = x[i/n][i%n];
		    x[i/n][i%n] =x[j/n][j%n];
		    x[j/n][j%n] = temp;
		}
	}
}
}
void mangGiamDan(int x[100][100], int m, int n){
	int k =m*n;
	for(int i=0; i<k-1; i++){     //duyet het tat cac ca phan tu trong mang 1 chieu
		for(int j=i+1; j<k; j++){
			if(x[i/n][i%n] < x[j/n][j%n]){ //vi tri ch�ng ta dang dung o dau
			int temp = x[i/n][i%n];
		    x[i/n][i%n] =x[j/n][j%n];
		    x[j/n][j%n] = temp;
		}
	}
}
}

int main(){
	nhapMaTran(a, m, n);
	xuatMaTran(a, m, n);
	mangTangDan(a,m,n);
	printf("\n mang da sap xep tang la : \n"); 
	xuatMaTran(a, m, n);
	mangGiamDan(a,m,n);
	printf("\n mang da sap xep giam la : \n"); 
	xuatMaTran(a, m, n);
}
