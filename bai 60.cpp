// cach xoa di 1 dong hoac 1 cot trong mang hai chieu
#include <stdio.h>
int a[100][100];
int m, n,k;

void nhapMaTran(int x[100][100], int &m, int &n){
	do{
		printf("Nhap vao m va n: ");
		scanf("%d%d", &m, &n);
	}while(m<=0 || n<=0);
	
	for(int i=0; i<m; i++){
		for(int j=0; j<n; j++){
			printf("x[%d][%d]=", i, j);
			scanf("%d", &x[i][j]);
		}
	}
}

void xuatMaTran(int x[100][100], int m, int n){
	printf("\nMang: \n");
	for(int i=0; i<m; i++){
		for(int j=0; j<n; j++){
			printf("%d\t", a[i][j]);
		}
		printf("\n");
	}
}

void xoaDongMaTran(int x[100][100], int &m, int n, int k){// luu y neu xoa 1 dong thi m se b? thay doi nen ta dung dau &
 if(k<0 || k>=m)
     return;
     /*
     1 2 3
     4 5 6
     7 8 9
     k=1
     1 2 3
     7 8 9
     */
  for(int i=k; i<m-1; i++){
  	for(int j=0; j<n;j++){
  		x[i][j]=x[i+1][j];
	  }
  }
  m = m-1;
  
}     
void xoaDongCotTran(int x[100][100], int m, int &n, int k){// luu y neu xoa 1 dong thi m se b? thay doi nen ta dung dau &
 if(k<0 || k>=n)
     return;
     /*
     1 2 3
     4 5 6
     7 8 9
     k=1
     1  3
     4  6
     7  9
     */
	for(int i=0; i<m; i++){
		for(int j=k; j<n-1; j++){
			x[i][j]=x[i][j+1];
		}
	}
	n = n - 1;
}
int main(){
	nhapMaTran(a, m, n);
	xuatMaTran(a, m, n);
	// xoaDongMaTran(a,m,n,1);
    xoaDongCotTran(a, m, n, 1);
	xuatMaTran(a, m, n);
}
