#include "stdio.h"
int main ()
{
   /* phan dinh nghia bien cuc bo  */
   int a = 1;

   /* phan thuc thi vong lap while */
   while( a < 20)
   {
      printf("Gia tri cua a la: %d\n", a);
      a++;
   }
   printf("\n===========================\n");
   printf("\n");
 
   return 0;
}

//vong lap for
#include <stdio.h>
 
int main ()
{
   /* phan thuc thi vong lap for */
   for( int a = 4; a < 16; a = a + 1 )
   {
      printf("Gia tri cua a la: %d\n", a);
   }
   printf("\n===========================\n");
   printf("VietJack chuc cac ban hoc tot! \n");
 
   return 0;
}
//do while

#include <stdio.h>
 
int main ()
{
   /* phan dinh nghia bien cuc bo  */
   int i, j;
   
   for(i=2; i<50; i++) {
      for(j=2; j <= (i/j); j++)
        if(!(i%j)) break;     // neu co thua so, thi khong la so nguyen to
      if(j > (i/j)) printf("%d la so nguyen to\n", i);
   }
   printf("\n===========================\n");
   printf("VietJack chuc cac ban hoc tot! \n");
 
   return 0;
}

